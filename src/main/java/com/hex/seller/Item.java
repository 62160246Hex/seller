/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.seller;

import java.io.Serializable;

/**
 *
 * @author Code.Addict
 */
public class Item implements Serializable {
	
	private String name, brand;
	private double price;
	private int amount, infoIndex;

	// Constructer :D
	Item(String name, String brand, double price, int amount){
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.amount = amount;
	}

	

	// Getter
	String getName(){
		return this.name;
	}

	String getBrand(){
		return this.brand;
	}

	double getPrice(){
		return this.price;
	}

	int getAmount(){
		return this.amount;
	}



	// Setter
	boolean modifyName(String name){
		this.name = name;
		return true;
	}

	boolean modifyBrand(String brand){
		this.brand = brand;
		return true;
	}

	boolean modifyPrice(double price){
		this.price = price;
		return true;
	}

	boolean modifyAmount(int amount){
		this.amount = amount;
		return true;
	}

	void setInfoIndex(int infoIndex){
		this.infoIndex = infoIndex;
	}


	@Override
	public String toString() {
		return String.format("(%s) %s / %s / %.2f$  [%d]", this.infoIndex, this.name, this.brand, this.price, this.amount);
	}
}
