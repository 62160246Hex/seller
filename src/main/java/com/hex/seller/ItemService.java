/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.seller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Code.Addict
 */
public class ItemService {

	private static ArrayList<Item> listItemData = new ArrayList<>(){};
	

	// Array of Users
	static{
		load();
	}

	
	/* Save section */
	static void saveAll(){
		save();
	}



	/* Clear section */
	static void clearAllListData(){
		listItemData.clear();
	}



	/* Add Section */
	static boolean addItem(Item item){
		listItemData.add(item);
		return true;
	}	



	/* Remove Section */
	static boolean removeItem(Item item){
		listItemData.remove(item);
		return true;
	}
	static boolean removeItem(int itemIndex){
		listItemData.remove(itemIndex);
		return true;
	}



	/* Update Section */
	static boolean updateItem(int itemIndex, Item item){
		listItemData.set(itemIndex, item);
		return true;
	}



	/* Getter Section */
	static ArrayList<Item> getItem(){
		for(int i = 0; i < listItemData.size(); i++){
			listItemData.get(i).setInfoIndex(i+1);
		}
		return listItemData;
	}
	static Item getItem(int index){
		return listItemData.get(index);
	}

	static double getBillsPrice(){
		double sum = 0.00;
		for(Item iItem : listItemData){
			sum += iItem.getPrice() * iItem.getAmount();
		}
		return sum;
	}
	
	static int getBillsTotalItem(){
		int sum = 0;
		for(Item iItem : listItemData){
			sum += iItem.getAmount();
		}
		return sum;
	}


	// @ Save & Load Data
	private static void save(){

		File file = null;
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {
			file = new File("cachedStore.dat");
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(listItemData);
			oos.close();
			fos.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static void load(){

		File file = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;

		try {
			file = new File("cachedStore.dat");
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
			listItemData = (ArrayList<Item>)ois.readObject();
			ois.close();
			fis.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
		}
    }

}
